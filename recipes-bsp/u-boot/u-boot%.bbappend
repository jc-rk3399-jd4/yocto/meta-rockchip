# Copyright (C) 2019, Fuzhou Rockchip Electronics Co., Ltd
# Released under the MIT license (see COPYING.MIT for the terms)

PATCHPATH = "${CURDIR}/u-boot"
inherit auto-patch

PV = "2017.09+git${SRCPV}"

LIC_FILES_CHKSUM = "file://Licenses/README;md5=a2c678cfd4a4d97135585cad908541c6"

inherit freeze-rev


SRCREV = "c83e7a17b25c05a3005e31c2f51bc7769536a9d1"
SRC_URI = " \
	git://github.com/T-Firefly/linux-mirrors.git;branch=uboot/rk3399/firefly; \
"
